﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScoopPractice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            //Get the item selected
            ListViewItem item = (ListViewItem)lvAvailable.SelectedItem;
            if (item == null)
            {
                return;
            }
            //get the string content of the item
            string value = item.Content+"";
            //create a new list item for the other list
            ListViewItem lvItem = new ListViewItem();
            //add this string value to the other list
            lvItem.Content = value;
            //Add this item to the list
            if (lvSelected.Items.Count <3) { 

                lvSelected.Items.Add(lvItem);}

            //if flavours more than 3 message too mant flavours selected
            else {
                MessageBox.Show("You have chosen the max number of scoops");
               
            }

        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            //Get the index of the selected item
            int index = lvSelected.SelectedIndex;
            //if nothing is selected
            if (index < 0)
            {
                return;
            }
            //remove the item from the list at this index
            lvSelected.Items.RemoveAt(index);
           

        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            lvSelected.Items.Clear();

        }
    }
}
