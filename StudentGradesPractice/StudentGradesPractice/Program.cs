﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentGradesPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                /*string[] lines = { "Jerry:A-,B+,C,F", "Tom:B-", "Maria:B+,A+,D+", "Eva:A-,B-,B+" };
                System.IO.File.WriteAllLines(@"..\..\grades.txt", lines);*/
string [] gradesLines= System.IO.File.ReadAllLines(@"..\..\grades.txt");
            
            

            
            double gpa=0.00;
            double sum = 0.00;
                String data = "";

                foreach (String line in gradesLines)
                {

                    String[] lineData = line.Split(new char[] { ':', ',' });
                    for (int i = 1; i < lineData.Length; i++)
                    {

                        sum += letterToNumberGrade(lineData[i]);


                    }
                    //Length-1 because we are not counting index 0 which is the name
                    gpa = sum / (line.Length - 1);
                    Console.WriteLine(lineData[0] + " has a gpa of vbbb " + gpa.ToString("#.##"));
                    data += lineData[0] + " has a gpa of" + gpa.ToString("#.##") + "\r\n";
                    //String[] data = new String[gradesLines.Length];
                    /*
                    for (int i=0; i< gradesLines.Length; i++){
                        //data[i] = lineData[0] + " has a gpa of" + gpa.ToString("#.##");
                        
                        
                    }*/
                    
                    //saving to new file
                   
                }
                try
                {

                    File.WriteAllText("output.txt", data);
                }
                catch (IOException ex)
                {
                    Console.Write("Error occured when writing data\n" + ex.Message);
                }
                Console.ReadLine();
            }
            catch (IOException ex) {
                Console.WriteLine("File cannot be read" + ex);
            }

        }//end of main
        static double letterToNumberGrade(string strGrade) {
            switch (strGrade) {
                case "A+":
                    return 4.00;
                case "A-":
                    return 3.67;
                case "B+":
                    return 3.33;
                case "B":
                    return 3.00;
                case "B-":
                    return 2.67;
                case "C+":
                    return 2.33;
                case "C":
                    return 2.00;
                case "C-":
                    return 1.67;
                case "D+":
                    return 1.33;
                case "D":
                    return 1.00;
                case "D-":
                    return 0.67;
                case "F":
                    return 0.00;
                default:
                    return 0.00;


            }


        }


    }
}
