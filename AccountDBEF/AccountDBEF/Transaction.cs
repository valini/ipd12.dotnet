﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AccountDBEF
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }
        
        public decimal Deposit { get; set; }

        public decimal Withdrawal { get; set; }

        public DateTime TransactionDate { get; set; }

    }
}
