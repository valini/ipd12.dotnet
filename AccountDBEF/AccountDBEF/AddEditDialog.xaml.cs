﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AccountDBEF
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        DatabaseContext ctx;
        private Transaction currentTransaction;
        public AddEditDialog(Transaction t)
        {
            currentTransaction = t;
            ctx = new DatabaseContext();
            InitializeComponent();
            if (currentTransaction == null)
            {

                btSave.Content = "Add New";
            }
            else
            {
                lblId.Content = t.TransactionId + "";

                if (t.Deposit == 0)
                {
                    rbDeposit.IsChecked = false;

                }
                else {
                    tbAmt.Text = t.Deposit+"";
                    rbDeposit.IsChecked = true;

                }
                if (t.Withdrawal == 0)
                {
                    rbWithdrawal.IsChecked = false;

                }
                else
                {
                    tbAmt.Text = t.Withdrawal + "";
                    rbWithdrawal.IsChecked = true;

                }




                dpTransactionDate.SelectedDate = t.TransactionDate;
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Transaction t = (currentTransaction == null ? new Transaction() : currentTransaction);


                if (!(decimal.TryParse(tbAmt.Text, out decimal amount)))
                {

                    MessageBox.Show("Invalid Amount entered.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;

                }

                if (amount < 0 || amount > 150)
                {

                    MessageBox.Show("Please enter a value between 0 and 150.", "Message", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;

                }
                decimal deposit = 0;
                decimal withdrawal = 0;

                if (rbDeposit.IsChecked == true)
                {

                    deposit = amount;

                }
                else if (rbWithdrawal.IsChecked == true)
                {

                    withdrawal = amount;
                }

                if (dpTransactionDate.SelectedDate == null)
                {
                    MessageBox.Show("Please pick a date");
                    return;
                }

                DateTime transactionDate = (DateTime)dpTransactionDate.SelectedDate;

                if (currentTransaction == null)
                {//add-insert
                    t = new Transaction { Deposit = deposit, Withdrawal = withdrawal, TransactionDate = transactionDate };
                    ctx.Transactions.Add(t); // persist - schedule Person to be inserted into database table
                    ctx.SaveChanges(); // flush - force all scheduled operations to be carried out in database
                }


                else
                {//update


                    string TransactionIdS = lblId.Content.ToString();

                    int.TryParse(TransactionIdS, out int TransactionId);
                    var transactions = (from r in ctx.Transactions where r.TransactionId == TransactionId select r).ToList();
                    //if there is no data with this id in the database
                    if (transactions.Count == 0)
                    {
                        MessageBox.Show("Record for update not found");

                        //Console.WriteLine("Record for update not found");
                    }
                    else
                    {//else there is one record in the database
                        t = transactions[0];
                        if (rbDeposit.IsChecked == true)
                        {

                            t.Deposit = decimal.Parse(tbAmt.Text);
                            t.Withdrawal = 0;

                        }
                        else if (rbWithdrawal.IsChecked == true)
                        {

                            t.Withdrawal = decimal.Parse(tbAmt.Text);
                            t.Deposit = 0;
                        }


                        t.TransactionDate = (DateTime)dpTransactionDate.SelectedDate;
                        // entity is tracked/attached, meaning this will cause an update to be scheduled-saveChanges means the update is waiting or scheduled to be flushed or executed
                        ctx.SaveChanges();


                    }

                }
                DialogResult = true;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Invalid data entered " + ex.Message);
            }


        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
