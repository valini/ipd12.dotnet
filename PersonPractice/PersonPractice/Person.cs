﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonPractice
{
    
        class Person
        {
        private int _id;
        private string _name;
        private string _age;
        private string _pets;
        private string _continents;
        private double _height;


        public Person(int id, string name, string age, string pets, string continents, double height)
            {
                this.ID = id;
                this.Name = name;
                this.Age = age;
                this.Pets = pets;
                this.Continents = continents;
                this.Height = height;
            }
 
            public int ID
            {
           
                get
                {

                    return _id;
                }

                set
                {
                    _id = value;

                }

            }

            
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    if (value.Length < 2 || value.Length > 50)
                    {
                        throw new ArgumentException("Name must be 2-50 characters long");
                    }
                    _name = value;
                }
            }
        
            public string Age
            {
                get
                {
                    return _age;
                }
                set
                {
                    
                    _age = value;
                }
            }
        public string Pets
        {
            get
            {
                return _pets;
            }
            set
            {
               
                _pets = value;
            }
        }
        public string Continents
        {
            get
            {
                return _continents;
            }
            set
            {

                _continents = value;
            }
        }
        public double Height
            {
                get
                {
                    return _height;
                }
                set
                {
                    if (value < 30 || value > 300)
                    {
                        throw new ArgumentException("Height must be 30-300");
                    }
                    _height = value;
                }
            }

        }


    }

