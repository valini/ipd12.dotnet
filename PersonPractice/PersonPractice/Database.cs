﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonPractice
{
    class Database
    {
        SqlConnection conn;
        public Database()
        {
            /* conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Valini\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30");
             conn.Open();*/
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "exercisesdb.database.windows.net";
            builder.UserID = "dbadmin";
            builder.Password = "Root12345";
            builder.InitialCatalog = "people";
            conn = new SqlConnection(builder.ConnectionString);
            conn.Open();
        }


        public List<Person> GetAllPeople()
        {

            List<Person> peopleList = new List<Person>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM People1", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["Name"];
                    string age = (string)reader["Age"];
                    string pets = (string)reader["Pets"];
                    string continents = (string)reader["Continents"];
                    double height = (double)reader["Height"];
                    Person person = new Person(id, name, age,pets, continents, height);
                    peopleList.Add(person);


                }
            }
            return peopleList;


        }
        public void AddPerson(Person person)
        {
            // insert a records
            SqlCommand insertCommand = new SqlCommand("INSERT INTO People1 (Name, Age, Pets, Continents, Height) VALUES (@Name, @Age,@Pets, @Continents, @Height)", conn);
            insertCommand.Parameters.AddWithValue("@Name,", person.Name);
            insertCommand.Parameters.AddWithValue("@Age,", person.Age);
            insertCommand.Parameters.AddWithValue("@Pets,", person.Pets);
            insertCommand.Parameters.AddWithValue("@Continents,", person.Continents);
            insertCommand.Parameters.AddWithValue("@Height,", person.Height);

            insertCommand.ExecuteNonQuery();
        }
        public int AddPersonID(Person p)
        {
            string sql = "INSERT INTO People1 (Name, Age, Pets, Continents, Height) VALUES (@Name, @Age,@Pets, @Continents, @Height);  " +
                "SELECT CONVERT(int, SCOPE_IDENTITY());";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@Name", p.Name);
                cmd.Parameters.AddWithValue("@Age", p.Age);
                cmd.Parameters.AddWithValue("@Age", p.Pets);
                cmd.Parameters.AddWithValue("@Age", p.Continents);
                cmd.Parameters.AddWithValue("@Height", p.Height);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }



    }
}
