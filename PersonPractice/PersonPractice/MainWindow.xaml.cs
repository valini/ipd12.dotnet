﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PersonPractice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database db;
        public MainWindow()
        {
            try
            {
                db = new Database();
                InitializeComponent();
                refreshPeopleList();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }

        private void refreshPeopleList()
        {
            lvPeople.ItemsSource = db.GetAllPeople();
            // Refresh not needed, when assigning ItemsSource Refresh is triggered
            // lbPeople.Items.Refresh();
        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lvPeople.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Person p = (Person)lvPeople.Items[index];
            lblId.Content = p.ID + "";
            tbName.Text = p.Name;
            if (p.Age == "Below 18")
            {
                rbBelow18.IsChecked = true;
            }
            else if (p.Age == "18 to 35")
            {
                rb18to35.IsChecked = true;
            }
            else {
                rbAbove35.IsChecked = true;

            }

            if (p.Pets.Contains("Dog")) {
                cbDog.IsChecked = true;
            }

            if (p.Pets.Contains("Cat"))
            {
                cbCat.IsChecked = true;
            }

            if (p.Pets.Contains("Other"))
            {
                cbOther.IsChecked = true;
            }
            cmbContinents.Text = p.Continents;
            slHeight.Value = p.Height;

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;

                String age = "";
                if (rbBelow18.IsChecked==true)
                {
                    age = "Below 18";
                }
                if (rb18to35.IsChecked==true)
                {
                    age = "18 to 35";
                }
                if (rbAbove35.IsChecked==true)
                {
                   age= "Above 36";
                }

                String pets = "";
                if (cbCat.IsChecked==true)
                {
                    pets = pets + "" + "Cat";

                }
                //if (cbDog.isSelected()){
                //checkedPets = checkedPets + " " +cbDog.getText()+ ",";
                if (cbDog.IsChecked == true)
                {
                    if (pets.Length > 0)
                    {
                        pets = pets + "," + "Dog";
                    }
                    else
                    {
                        pets = "Dog";
                    }
                    //checkedPets = checkedPets + " " +cbDog.getText()+ ",";

                }
                if (cbOther.IsChecked == true)
                {

                    if (pets.Length > 0)
                    {
                        pets = pets + "," + "Other";
                    }
                    else
                    {
                       pets = "Other";
                    }

                }

                string continents = cmbContinents.Text;
                double height = slHeight.Value;
                Person p = new Person(0, name, age, pets, continents,height);
                db.AddPersonID(p);
                refreshPeopleList();


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Invalid data entered " + ex.Message);
            }




        }


    }
}
