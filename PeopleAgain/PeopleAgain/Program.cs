﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace PeopleAgain
{
    class Program
    {
        class Person
        {
            private string _name;
            private int _age;

            public Person(String name, int age)
            {
                _name = name;
                _age = age;
            }

            public int Age
            {
                get
                {
                    return _age;
                }
                set
                {
                    if (value < 0 || value > 150)
                    {
                        throw new InvalidDataException("Age must be between 0 and 150");
                    }
                    _age = value;
                }
            }

            public String Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    if (!Regex.Match(_name, "^[a-zA-Z. -]{ 1,20}$").Success)

                    {
                        throw new InvalidDataException("Name must be between 1 and 20, include lowercase, uppercase, dots, hyphens and space");
                    }
                    _name = value;
                }


            }
            public override string ToString()
            {
                return "Person: " + Name + " " + Age;
            }

        }//end of class Person

        class Student : Person
        {
            private double _gpa;
            private string _program;

            public Student(String name, int age, double gpa, string program) : base(name, age)
            {
                _gpa = gpa;
                _program = program;
            }

            public double GPA
            {
                get
                {
                    return _gpa;
                }
                set
                {
                    if (value < 0 || value > 4.3)
                    {
                        throw new InvalidDataException("GPA must be between 0 and 4.3");
                    }
                    _gpa = value;
                }
            }

            public String Program
            {
                get
                {
                    return _program;
                }
                set
                {
                    if (!Regex.Match(_program, "^[a-zA-Z]{ 1,20}$").Success)

                    {
                        throw new InvalidDataException("Program must be between 1 and 20 characters long");
                    }
                    _program = value;
                }
            }
            public override string ToString()
            {
                return "Student: " + Name + " " + Age + " " + GPA + " " + Program;
            }

        }//end of class Student
        class Teacher : Person
        {
            private string _subject;
            private int _yearsOfExperience;

            public Teacher(String name, int age, string subject, int yearsOfExperience) : base(name, age)
            {
                _subject = subject;
                _yearsOfExperience = yearsOfExperience;
            }

            public string Subject
            {
                get
                {
                    return _subject;
                }
                set
                {
                    if (!Regex.Match(_subject, "^[a-zA-Z]{ 1,20}$").Success)

                    {
                        throw new InvalidDataException("Program must be between 1 and 20 characters long");
                    }
                    _subject = value;
                }
            }

            public int YearsOfExperience
            {
                get
                {
                    return _yearsOfExperience;
                }
                set
                {
                    if (value < 0 || value > 100)
                    {
                        throw new InvalidDataException("Years of Experience must be between 0 and 100");
                    }
                    _yearsOfExperience = value;
                }
            }
            public override string ToString()
            {
                return "Teacher: " + Name + " " + Age + " " + Subject + " " + YearsOfExperience;
            }

        }//end of class Teacher

        static void Main(string[] args)
        {
            List<Person> PeopleList = new List<Person>();
            string[] linesArray = File.ReadAllLines(@"..\..\people.txt");
            //|| (!line.StartsWith("Student:")) || (!line.StartsWith("Teacher:"))
            foreach (string line in linesArray)
            {
                if (line.Equals(""))
                {
                    Console.WriteLine("Empty Line!!!");
                    continue;

                }
                string[] splitOne = line.Split(':');
                string className = splitOne[0];
                string[] lineElements = splitOne[1].Split(',');
                //foreach (string Elements in lineElements)
                if (className == ("Person"))
                {
                    if (lineElements.Length == 2)
                    {

                        String name = lineElements[0];
                        //int age = int.Parse(lineElements[1]);
                        int age;
                        if (int.TryParse(lineElements[1], out age) == false) {
                            Console.WriteLine("This is not an integer");
                        }
                        

                        Person person = new Person(name, age);
                        PeopleList.Add(person);
                    }
                    else
                    {
                        Console.WriteLine("Missing data or extra data in line");

                    }
                }


                else if (className == ("Student"))
                {
                    if (lineElements.Length == 4)
                    {

                        String name = lineElements[0];
                        int age = int.Parse(lineElements[1]);
                        //double gpa = double.Parse(lineElements[2]);
                        double gpa;
                        if (double.TryParse(lineElements[2], out gpa) == false)
                        {
                            Console.WriteLine("This is not a double");
                        }
                        string program = lineElements[3];
                        Student student = new Student(name, age, gpa, program);
                        PeopleList.Add(student);
                    }


                    else
                    {
                        Console.WriteLine("Missing data or extra data in line");

                    }
                }



                if (className == ("Teacher"))
                {
                    if (lineElements.Length == 4)

                    {
                        String name = lineElements[0];
                        int age = int.Parse(lineElements[1]);
                        string subject = lineElements[2];
                        int gpa = int.Parse(lineElements[3]);

                        Teacher teacher = new Teacher(name, age, subject, gpa);
                        PeopleList.Add(teacher);
                    }
                    else
                    {
                        Console.WriteLine("Missing data or extra data in line");

                    }
                }


                //}



            }
            PeopleList.Sort((p1, p2) => p1.Name.CompareTo(p2.Name));
            foreach (Person p in PeopleList)
            {
                Console.WriteLine(p.ToString());
            }


            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }
    }
}

