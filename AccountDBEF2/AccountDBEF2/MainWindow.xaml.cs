﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccountDBEF2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DatabaseContext ctx;
        public MainWindow()
        {
            ctx = new DatabaseContext();
            InitializeComponent();
            refreshTransactionsList();
        }

        private void refreshTransactionsList()
        {
            //select GetAllMethod
            var transactions = (from r in ctx.Transactions select r).ToList();
            lvTransactions.ItemsSource = transactions;
            lblBalance.Content = getBalance();
            lblCursorPosition.Text = ("Balance $" + lblBalance.Content + " on " + lvTransactions.Items.Count + " transaction(s)");


        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDialog = new AddEditDialog(null);

            if (addEditDialog.ShowDialog() == true)
            {

                refreshTransactionsList();
            }
        }

        private void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFileSaveAs_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFileSave_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void lvTransaction_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            Transaction t = (Transaction)lvTransactions.SelectedItem;
            if (t == null)
            {
                return;
            }
            AddEditDialog addEditDialog = new AddEditDialog(t);
            if (addEditDialog.ShowDialog() == true)
            {

                refreshTransactionsList();
            }

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            int index = lvTransactions.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Transaction transaction = (Transaction)lvTransactions.Items[index];
            try
            {
                int TransactionId = transaction.TransactionId;

                //int.TryParse(PersonIdS, out int PersonId);

                var transactions = (from r in ctx.Transactions where r.TransactionId == TransactionId select r).ToList();
                //if there is no such id in database
                if (transactions.Count == 0)
                {
                    Console.WriteLine("Record for deletion not found");
                }
                else
                {
                    Transaction p = transactions[0];
                    ctx.Transactions.Remove(p);
                    ctx.SaveChanges();

                }

                refreshTransactionsList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }


        }
        public decimal getBalance()
        {
            decimal balance = 0;
            var transactions = (from r in ctx.Transactions select r).ToList();
            foreach (var tt in transactions)
            {
                balance += (tt.Deposit - tt.Withdrawal);

            }
            return balance;
        }
    }
}
