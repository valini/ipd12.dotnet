﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace AccountDBEF2
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=AccountsDatabase")
        {
        }

        public virtual DbSet<Transaction> Transactions { get; set; }



    }
}
