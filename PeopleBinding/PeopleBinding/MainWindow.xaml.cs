﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeopleBinding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static int id=0;
        List<Person> people = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();
            



        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            
            String name = tbName.Text;
            int age;
            int.TryParse(tbAge.Text, out age);
             id++;
            Console.WriteLine("the is id" +id);

            //people.Add(new Person(name, age));
            people.Add(new Person(id,name, age));
            lvPeople.ItemsSource = people;
            lvPeople.Items.Refresh();
            tbName.Text = "";
            tbAge.Text = "";

        }

        private void DeleteItem_Click(object sender, RoutedEventArgs e)
        {
           //var selectedItem = lvPeople.SelectedItem;
            //lvPeople.Items.Remove(selectedItem);
            //lvPeople.Items.Remove(selectedItem);
            //lvPeople.Items.Refresh();

           // ((List<Person> )lvPeople.ItemsSource).Remove(selectedItem);

            people.RemoveAt(lvPeople.SelectedIndex);
            lvPeople.Items.Refresh();
        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            
            var selectedPerson = (Person)lvPeople.SelectedItem;

            if (selectedPerson == null) {
                lblID.Content ="...";
                return;
            }
            tbName.Text = selectedPerson.Name;
            int age = selectedPerson.Age;
            tbAge.Text = age +"";
            lblID.Content = selectedPerson.ID;
            //Person people = (Person)lvPeople.SelectedItem;
            //tbName.Text = lvPeople.Name;
            //tbAge.Text = lvPeople.Age;
            
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            Person selectedPerson = people[lvPeople.SelectedIndex];
            selectedPerson.Name = tbName.Text;
             int.TryParse(tbAge.Text,out int age)  ;
            selectedPerson.Age = age;
            //tbAge.Text = age + "";
            lvPeople.Items.Refresh();


        }
    }
    class Person
    {
        private int _id;
        private string _name;
        private int _age;

        public Person(int id, String name, int age)
        {
            ID = id;
            Name = name;
            Age = age;
        }


        public Person( String name, int age)
        {
            
            Name= name;
            Age = age;
        }

        public  int ID {



            get {
                //_id++;
                return _id;
            }

            set {
                _id = value;

            }

        }
        public String Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z]{2,50}$").Success)

                {
                    throw new InvalidDataException("Name must be between 2 and 50 characters");
                }
                _name = value;
            }


        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new InvalidDataException("Age must be between 0 and 150");
                }
                _age = value;
            }
        }

      

    }//end of class Person
}
