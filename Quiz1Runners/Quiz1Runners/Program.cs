﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1Runners
{
    class Runner
    {
        public string _name; // 2-20 characters long
        public double _avgTime; // 0 or more

        public Runner(String name, double avgTime)
        {
            _name = name;
            _avgTime = avgTime;
        }

        public String Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (!Regex.Match(_name, "^[a-zA-Z]{ 1,20}$").Success)

                {
                    throw new InvalidDataException("Name must be between 1 and 20 long");
                }
                _name = value;
            }


        }
        public double AvgTime
        {
            get
            {
                return _avgTime;
            }
            set
            {
                if (value < 0)
                {
                    throw new InvalidDataException("Average Time must be between 0 or more");
                }
                _avgTime = value;
            }
        }

        public override string ToString()
        {
            return Name + " ran " + AvgTime + "times ";
        }


    }


    class Program
    {
        static List<double> runtimesList = new List<double>(); // keep as field, not property

static List<Runner> _runnerList = new List<Runner>();
        static void Main(string[] args)
        {
            
            try
            {
                string[] lines = File.ReadAllLines(@"..\..\runners.txt");
                
                double avgTime;
                double time;
                string name = "";
                double sum = 0;

                
                foreach (String line in lines)
                {List<double> timelist = new List<double>();
                    if (double.TryParse(line, out avgTime) == false)
                    {


                        name = line;
                        Console.WriteLine(name);


                        if (double.TryParse(line, out time) == true)
                        {
                            timelist.Add(time);
                            sum += time;
                            Console.WriteLine(sum);
                            avgTime = sum / timelist.Count; }

if (double.TryParse(line, out time) == true) && line[i+1]
                            Runner runner = new Runner(name, avgTime);

                            Console.WriteLine("avgtime" + avgTime);
                            break;

                        }
                    }

                  
                    runtimesList.Add(avgTime);
                    _runnerList.Add(new Runner(name, avgTime));

                }

                //Average runtimes
                double avgruntimes = 0.0;
                double sumruntimes = 0.0;
                foreach (int i in runtimesList)
                {
                    sumruntimes += i;

                }
                avgruntimes = sumruntimes / runtimesList.Count;
                Console.WriteLine("The average time for all runners was" + avgruntimes);
                //fastest time
                runtimesList.Sort();
                double fastestTime = runtimesList[0];




                //LINQ sorting
                var listByName = from r in _runnerList orderby r.Name select r;

                List<Runner> runnerListByName = listByName.ToList();

                Console.WriteLine("===== BY NAME =======");
                foreach (Runner r in listByName)
                {
                    Console.WriteLine("{0} ran {1} seconds", r.Name, r.AvgTime);
                }

                //Output
                Console.WriteLine("The average time for all runners was" + avgruntimes);
                Console.WriteLine("The fastest runner was" + fastestTime);

            }
            catch (IOException ex)
            {
                Console.Write("Error occured when writing data\n" + ex.Message);
            }

            Console.ReadLine();
        }
    }

}
    


