﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SharpNote
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        String currentFile = "";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
           
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    currentFile = openFileDialog.FileName;
                    tbDocument.Text = File.ReadAllText(currentFile);
                    lblCursorPosition.Text = currentFile;
                }
                catch (IOException ex) {
                    MessageBox.Show("Error Opening File"+ex.Message, "File Open error", MessageBoxButton.OK, MessageBoxImage.Error);

                }
            }
                
        }
    }
}
