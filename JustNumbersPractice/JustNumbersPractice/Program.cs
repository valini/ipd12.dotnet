﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNumbersPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            while (true) { 
            Console.WriteLine("Please enter a positive integer. Press 0 to end");
                string line = Console.ReadLine();
                int value;
                if (int.TryParse(line, out value) == false)
                {
                    Console.WriteLine("This is not an integer");
                    continue;
                }
                else if (value <= 0) {
                    Console.WriteLine("Done entering data");
                    break;

                }
                list.Add(value);
                }//end of while
            //avg
            double avg;
            double sum=0;
            foreach (int i in list) {
                  sum += i;

            }
            avg = sum / list.Count();
            Console.WriteLine("The average of the numbers is {0}", avg);
            //Maximum
            list.Sort();
            int Max = list[list.Count()-1];
            Console.WriteLine("The maximum number is {0}", Max);
            //Median
            double median;
            if (list.Count() % 2 == 1) {
                median = list[list.Count / 2];
            }
            else
            {
                median = (list[list.Count / 2] + list[list.Count / 2 - 1]) / 2.0;

            }
            Console.WriteLine("The median of the numbers is {0}", median);
            //standard deviation

            double variance=0.0;

            foreach (int i in list) {
               variance+= (Math.Pow(i - avg, 2))/list.Count;
            }

            double sd = Math.Pow(variance, 0.5);
            Console.WriteLine("The Standard Deviation is {0}", sd);


            

            //save the string list in a text file
            
            try {
                string data = String.Join(";",list);
                File.WriteAllText("data.text", data);


            }
            catch(IOException ex) {
                Console.WriteLine("Error saving file"+ ex);

            }
            Console.ReadLine();
        }
        
        }
    
    }


