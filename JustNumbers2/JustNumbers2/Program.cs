﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustNumbers2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            while (true)
            { 
            Console.WriteLine("Please enter a positive num. enter 0 to terminate");
            string line = Console.ReadLine();
            int value = 0;
            if (int.TryParse(line, out value) == false)
            {
                Console.WriteLine("Not a valid integer, try again");
                continue;

            }
            if (value <= 0)
            {
                Console.WriteLine("Done entering data");
                break;
            }
                list.Add(value);

            }


        }
    }
}
