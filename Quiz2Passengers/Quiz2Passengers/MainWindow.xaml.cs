﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                Globals.db = new Database();
                InitializeComponent();
                refreshPassengerList();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }

        private void refreshPassengerList()
        {
            lvPassengers.ItemsSource = Globals.db.GetAllPassengers();
            // Refresh not needed, when assigning ItemsSource Refresh is triggered
            // lbPeople.Items.Refresh();
        }



        private void lvPassengers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passenger passenger = (Passenger)lvPassengers.SelectedItem;
            if (passenger == null)
            {
                return;
            }
            InputDialog dlg = new InputDialog(passenger);
            if (dlg.ShowDialog() == true)
            {

                lvPassengers.ItemsSource = Globals.db.GetAllPassengers();
            }

        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Passenger> passengerList = Globals.db.GetAllPassengers();
            String word = tbSearch.Text;
            if (word != "")
            {
                var result = from t in passengerList where t.Name.Contains(word) select t;
                passengerList = result.ToList();

            }
            lvPassengers.ItemsSource = passengerList;

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {

            InputDialog dlg = new InputDialog(null);
            if (dlg.ShowDialog() == true)
            {

                lvPassengers.ItemsSource = Globals.db.GetAllPassengers();
            }

        }

        private void btSort_Click(object sender, RoutedEventArgs e)
        {
            SortInputDialog dlg = new SortInputDialog();
            if (dlg.ShowDialog() == true)
            {

               
            }

        }
    }
}
