﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Names;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
      private Passenger currentItem;
        public InputDialog(Passenger Item)
        {
            currentItem = Item;
            InitializeComponent();
            if (currentItem == null)
            {

                btSave.Content = "Add New";
                btDelete.Visibility = Visibility.Hidden;




            }
            else
            {
                lblId.Content = Item.Id + "";
                tbPassport.Text = Item.Passport;
                tbDestination.Text = Item.Destination;

                dpDepartureDate.SelectedDate = Item.DepartureDateTime;
                
                  string time = Item.DepartureDateTime.ToString("HH:mm");
                cmbDepartureTime.Text = time;
              


            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            Passenger passenger = (currentItem == null ? new Passenger() : currentItem);
            //Passenger passenger = new Passenger();
            passenger.Name = tbName.Text;
            passenger.Passport = tbPassport.Text;
            passenger.Destination = tbDestination.Text;
            if (dpDepartureDate.SelectedDate == null)
            {
                MessageBox.Show("Please pick a date");
                return;
            }

            passenger.DepartureDateTime = (DateTime)dpDepartureDate.SelectedDate;
            
            if (currentItem == null)
            {//add-insert

                Globals.db.AddPassenger(passenger);
            }
            else
            {//update

                Globals.db.UpdatePassenger(passenger);
            }
            DialogResult = true;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
           //int id = int.Parse(lblId.Content);
           // Globals.db.DeletePassengerById(id);

        }
    }
}
