﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
//using System.Threading.Names;

namespace Quiz2Passengers
{
    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Valini\Desktop\ipd12_Net\Quiz2Passengers\Quiz2Passengers\Quiz2PassengersDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public int AddPassenger(Passenger p)
        {
            string sql = "INSERT INTO Passengers (Name,Passport, Destination, DepartureDateTime) VALUES (@Name, @Passport, @Destination,  @DepartureDateTime);  " +
                "SELECT CONVERT(int, SCOPE_IDENTITY());";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@Name", p.Name);
                cmd.Parameters.AddWithValue("@Passport", p.Passport);
                cmd.Parameters.AddWithValue("@Destination", p.Destination);
                cmd.Parameters.AddWithValue("@DepartureDateTime", p.DepartureDateTime);
                
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }

        public List<Passenger> GetAllPassengers()
        {
            List<Passenger> result = new List<Passenger>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Passengers", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string Name = (string)reader["Name"];
                    string Passport = (string)reader["Passport"];
                    string Destination = (string)reader["Destination"];
                    DateTime DepartureDateTime = (DateTime)reader["DepartureDateTime"];
                   
                    Passenger item = new Passenger(id, Name,Passport,Destination, DepartureDateTime);
                  //  Passenger item = new Passenger() { Id = id, Name = Name, Passport=Passport,Destination=Destination, DepartureDateTime = DepartureDateTime  };
                    result.Add(item);
                }
            }
            return result;
        }


        internal void UpdatePassenger(Passenger p)
        {
            string sql = "UPDATE Passengers SET Name = @Name, @Passport, @Destination, @DepartureDateTime = @DepartureDueDate,  WHERE Id=@ID";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@ID", p.Id);
            cmd.Parameters.AddWithValue("@Name", p.Name);
            cmd.Parameters.AddWithValue("@Passport", p.Passport);
            cmd.Parameters.AddWithValue("@Passport", p.Destination);
            cmd.Parameters.AddWithValue("@DepartureDateTime", p.DepartureDateTime);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        internal void DeletePassengerById(int id)
        {
            string sql = "DELETE FROM Passengers WHERE Id=@ID";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
    }
}
