﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    public class Passenger
    {
        public int _id;
        public string _name;
        public string _passport;
        public string _destination;
        public DateTime _departureDateTime;

        public Passenger(int id, string name, string passport, string destination, DateTime departureDateTime)
        {
            this.Id = id;
            this.Name = name;
            this.Passport = passport;
            this.Destination = destination;
            this.DepartureDateTime = departureDateTime;
        }

        public Passenger()
        {
            this.Id = Id;
            this.Name = Name;
            this.Passport = Passport;
            this.Destination = Destination;
            this.DepartureDateTime = DepartureDateTime;
        }



        public int Id
        {



            get
            {

                return _id;
            }

            set
            {
                _id = value;

            }

        }


        public string Name
        {
            get
            {
                return _name;
            }
            set
            {

                if (!Regex.Match(value, "^[a-zA-Z]{1,100}$").Success)
                {
                    throw new ArgumentException("Name must be 1-100 characters long");
                }
               

                _name = value;
            }
        }


        public string Passport
        {
            get
            {
                return _passport;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z][0-9]{6}$").Success)
                {
                    throw new ArgumentException("Passport number must be 8 characters  long starting with 2 letters followed by numbers");
                }
                _passport = value;
            }
        }

        public string Destination
        {
            get
            {
                return _destination;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z]{1,100}$").Success)
                {
                    throw new ArgumentException("Destination must be 1-100 characters long");
                }
                _destination = value;
            }
        }



        public DateTime DepartureDateTime
        {
            get
            {
                return _departureDateTime;
            }
            set
            {

                _departureDateTime = value;
            }
        }

       

    }
}

