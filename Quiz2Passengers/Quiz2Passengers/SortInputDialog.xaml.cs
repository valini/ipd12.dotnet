﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for SortInputDialog.xaml
    /// </summary>
    public partial class SortInputDialog : Window
    {
        public SortInputDialog()
        {
            InitializeComponent();
        }

        private void btCancelSort_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btSaveSort_Click(object sender, RoutedEventArgs e)
        {
            string sort;
            if (rbName.IsChecked == true) {
                sort = "Name";
            }
            else if (rbDestination.IsChecked == true)
            {
                sort = "Destination";
            }

        }
    }
}
