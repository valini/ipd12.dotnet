﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RegisterPeople
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }





        private void btRegister_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                String name = tbName.Text;

                if (name == "")
                {
                    MessageBox.Show("Please enter a valid name between 2 and 50", "Register People", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                else if (!Regex.Match(name, "^[a-zA-Z]{2,50}$").Success)

                {
                    //throw new System.IO.InvalidDataException("Name must be between 2 and 50 long");
                    MessageBox.Show("Please enter a valid name between 2 and 50", "Register People", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }








                String ageStr = tbAge.Text;
                /*if (ageStr == "")
                {
                    MessageBox.Show("Please enter a valid age", "Register People", MessageBoxButton.OK, MessageBoxImage.Information);
                }*/

                int.TryParse(ageStr, out int ageInt);
                String age = "";
                if (ageStr == "" || ageInt < 0 || ageInt > 150)
                {
                    MessageBox.Show("Age must be between 0 and 150", "Register People", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                else
                {
                    age = ageInt + "";

                }

                String gender = "";
                if (rbMale.IsChecked == true)
                {

                    gender = "Male";
                }

                else if (rbFemale.IsChecked == true)
                {

                    gender = "Female";
                }

                else if (rbNA.IsChecked == true)
                {

                    gender = "NA";
                }
                //pets

                String pets = "";
                if (cbDog.IsChecked == true)
                {

                    pets = "Dog";
                }



                if (cbCat.IsChecked == true)
                {

                    if (pets.Length > 0)
                    {

                        pets = pets + "," + "Cat";
                    }
               
                    else
                    {
                        pets = "Cat";
                    }
                }


                if (cbOther.IsChecked == true)
                {

                    if (pets.Length > 0)
                    {
                        pets = pets + "," + "Other";
                    }
                    else
                    {
                        pets = "Other";
                    }

                }

                

                //Continents
                string continents = cmbContinents.Text;

                //MessageBox.Show(name+age+gender+pets+continents, "My App", MessageBoxButton.OK, //MessageBoxImage.Information);

                try
                {
                    string[] lines = { name + ";" + age + ";" + gender + ";" + pets + ";" + continents };
                    File.AppendAllLines(@"..\..\output.txt", lines);
                    MessageBox.Show("File saved successfully", "Register People", MessageBoxButton.OK, MessageBoxImage.Information);
                    tbName.Text = "";
                    tbAge.Text = "";
                    rbMale.IsChecked = true;
                    cbDog.IsChecked = true;
                    cbCat.IsChecked = false;
                    cbOther.IsChecked = false;


                    cmbContinents.Text = "";

                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error writing files" + ex);
                }
                
            }
            catch (InvalidDataException ie)
            {

                MessageBox.Show("Invalid Input" + ie);
            }
        }
    }
}

