﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CustomPizza
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog()
        {
            InitializeComponent();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
           
        }

        public string Crust
        {
            get { return cmbCrust.Text; }
        }

        public string Sauce
        {
            get {
                
                String sauce = "";
                if (rbTomato.IsChecked == true)
                {

                    sauce = "Tomato";
                }

                else if (rbCheese.IsChecked == true)
                {

                    sauce = "Cheese";
                }

                else if (rbWine.IsChecked == true)
                {

                    sauce = "Wine";
                }
                return sauce;



                }
        }

        public string Topping
        {
            get {
                String topping = "";

                if (cbTomatoes.IsChecked == true)
                {

                    topping = "Tomatoes";
                }



                if (cbMushrooms.IsChecked == true)
                {

                    if (topping.Length > 0)
                    {

                        topping = topping + "," + "Mushrooms";
                    }

                    else
                    {
                        topping = "Mushrooms";
                    }
                }


                if (cbSpinach.IsChecked == true)
                {

                    if (topping.Length > 0)
                    {
                        topping = topping + "," + "Spinach";
                    }
                    else
                    {
                        topping = "Spinach";
                    }

                }
                return topping;
            }
        }

        private void dlgbtSave_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void dlgbtCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

      
    }
}
