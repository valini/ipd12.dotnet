﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace TodoDBCustDlg { 

    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Valini\Desktop\ipd12_Net\TodoDBCustDlg\TodoDBCustDlg\ToDoDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }

        public int AddTodo(Todo p)
        {
            string sql = "INSERT INTO Todos (Task, DueDate, IsDone) VALUES (@Task, @DueDate, @IsDone);  " +
                "SELECT CONVERT(int, SCOPE_IDENTITY());";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@Task", p.Task);
                cmd.Parameters.AddWithValue("@DueDate", p.DueDate);
                cmd.Parameters.AddWithValue("@IsDone", p.IsDone);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }

        public List<Todo> GetAllTodos()
        {
            List<Todo> result = new List<Todo>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Todos", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string Task = (string)reader["Task"];
                    DateTime DueDate = (DateTime)reader["DueDate"];
                    bool IsDone = (byte)reader["IsDone"]!=0;
                // Todo p = new Todo(id, Task, DueDate, IsDone);
                Todo item = new Todo() { Id = id, Task = Task, DueDate = DueDate, IsDone = IsDone };
                    result.Add(item);
                }
            }
            return result;
        }

        
        internal void UpdateTodo(Todo p)
        {
            string sql = "UPDATE Todos SET Task = @Task, DueDate = @DueDate, IsDone = @IsDone WHERE Id=@ID";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@ID", p.Id);
            cmd.Parameters.AddWithValue("@Task", p.Task);
            cmd.Parameters.AddWithValue("@DueDate", p.DueDate);
            cmd.Parameters.AddWithValue("@IsDone", p.IsDone);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        internal void DeleteTodoById(int id)
        {
            string sql = "DELETE FROM Todos WHERE Id=@ID";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
    }
}
