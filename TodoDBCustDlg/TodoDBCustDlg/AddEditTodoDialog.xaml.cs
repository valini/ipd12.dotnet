﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TodoDBCustDlg
{
    /// <summary>
    /// Interaction logic for AddEditTodoDialog.xaml
    /// </summary>
    public partial class AddEditTodoDialog : Window
    {
        private Todo currentItem;
        public AddEditTodoDialog(Todo Item)
        {
            currentItem = Item;
            InitializeComponent();
            if (currentItem == null)
            {

                btSave.Content = "Add New";
            }
            else {
                lblId.Content = Item.Id + "";
                tbTask.Text = Item.Task;
                dpDueDate.SelectedDate = Item.DueDate;
                cbDone.IsChecked = Item.IsDone;


            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            Todo todo = (currentItem == null ? new Todo() : currentItem);
            //Todo todo = new Todo();
                todo.Task = tbTask.Text;
            //FIXME..with if else if date is null
            if (dpDueDate.SelectedDate == null) {
                MessageBox.Show("Please pick a date");
                return;
            }
           
                todo.DueDate =(DateTime)dpDueDate.SelectedDate;
                todo.IsDone = (bool)cbDone.IsChecked;
            if (currentItem == null)
            {//add-insert
                
                Globals.db.AddTodo(todo);
            }
            else {//update

                Globals.db.UpdateTodo(todo);
            }
            DialogResult = true;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
