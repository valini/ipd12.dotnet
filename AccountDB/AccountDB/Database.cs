﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;


namespace AccountDB
{
    class Database
    {
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Valini\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();
        }


        public int AddTransaction(Transaction p)
        {
            string sql = "INSERT INTO Transactions (Deposit, Withdrawal, TransactionDate) VALUES (@Deposit, @Withdrawal, @TransactionDate);  " +
                "SELECT CONVERT(int, SCOPE_IDENTITY());";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@Deposit", p.Deposit);
                cmd.Parameters.AddWithValue("@Withdrawal", p.Withdrawal);
                cmd.Parameters.AddWithValue("@TransactionDate", p.TransactionDate);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }

        public List<Transaction> GetAllTransactions()
        {
            List<Transaction> result = new List<Transaction>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM Transactions", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int Id = (int)reader["Id"];
                    double Deposit = (double)reader["Deposit"];
                    double Withdrawal = (double)reader["Withdrawal"];
                    DateTime TransactionDate = (DateTime)reader["TransactionDate"];
                    
                    //Transaction item = new Transaction(Id, Deposit, //Withdrawal, TransactionDate);
                    Transaction item = new Transaction() { Id = Id, Deposit = Deposit, Withdrawal = Withdrawal, TransactionDate = TransactionDate };
                    result.Add(item);
                }
            }
            return result;
        }


        internal void UpdateTransaction(Transaction p)
        {
            string sql = "UPDATE Transactions SET Deposit = @Deposit, Withdrawal = @Withdrawal, TransactionDate = @TransactionDate WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@Id", p.Id);
            cmd.Parameters.AddWithValue("@Deposit", p.Deposit);
            cmd.Parameters.AddWithValue("@Withdrawal", p.Withdrawal);
            cmd.Parameters.AddWithValue("@TransactionDate", p.TransactionDate);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        internal void DeleteTransactionById(int id)
        {
            string sql = "DELETE FROM Transactions WHERE Id=@Id";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }

        public double getBalance()
        {
            string sql = "SELECT SUM(deposit-withdrawal) as sum FROM transactions";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            double balance = (double)cmd.ExecuteScalar();
            return balance;
        }

    }
}

