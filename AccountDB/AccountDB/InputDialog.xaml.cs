﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Deposits;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AccountDB
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        private Transaction currentItem;

        public InputDialog(Transaction Item)
        {
            currentItem = Item;
            InitializeComponent();
            if (currentItem == null)
            {

                btSave.Content = "Add New";
            }
            else
            {
                lblId.Content = Item.Id + "";
                if (Item.Deposit == 0)
                {
                    rbDeposit.IsChecked = false;

                }
                else {
                    tbAmt.Text = Item.Deposit + "";
                    rbDeposit.IsChecked = true;
                }
                if (Item.Withdrawal == 0)
                {
                    rbWithdrawal.IsChecked = false;

                }
                else
                {
                    tbAmt.Text = Item.Withdrawal + "";
                    rbWithdrawal.IsChecked = true;
                }

                dpTransactionDate.SelectedDate = Item.TransactionDate;
                


            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            //Transaction transaction = new Transaction();
            Transaction transaction = (currentItem == null ? new Transaction() : currentItem);
            double amount = double.Parse(tbAmt.Text);
            
        transaction.TransactionDate = (DateTime)dpTransactionDate.SelectedDate;
            

            if (rbDeposit.IsChecked==true)
            {
                transaction.Deposit = amount;
                transaction.Withdrawal = 0;
            }
            if (rbWithdrawal.IsChecked == true)
            {
                transaction.Deposit =0;
                transaction.Withdrawal = amount;
            }

           
           
         
            if (currentItem == null)
            {//add-insert

                Globals.db.AddTransaction(transaction);
            }
            else
            {//update

                Globals.db.UpdateTransaction(transaction);
            }
            DialogResult = true;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
