﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AccountDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                Globals.db = new Database();
                InitializeComponent();
                refreshTransactionList();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }
        private void refreshTransactionList()
        {
            lvTransactions.ItemsSource = Globals.db.GetAllTransactions();
            lblBalance.Content=Globals.db.getBalance();
            lblCursorPosition.Text= ("Balance $" + lblBalance.Content + " on " + lvTransactions.Items.Count + " transaction(s)");
           
            // Refresh not needed, when assigning ItemsSource Refresh is triggered
            // lbPeople.Items.Refresh();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
           InputDialog dlg = new InputDialog(null);
            if (dlg.ShowDialog() == true)
            {

                //lvTransactions.ItemsSource = Globals.db.GetAllTransactions();
                refreshTransactionList();
            }
        }
        private void lvTransaction_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {


            Transaction transaction = (Transaction)lvTransactions.SelectedItem;
            if (transaction == null)
            {
                return;
            }
            InputDialog dlg = new InputDialog(transaction);
            if (dlg.ShowDialog() == true)
            {

                lvTransactions.ItemsSource = Globals.db.GetAllTransactions();
            }

        }

        private void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFileSaveAs_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Transaction t = (Transaction)lvTransactions.SelectedItem;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
                if (saveFileDialog.ShowDialog() == true)
                    File.AppendAllText(saveFileDialog.FileName, t.ToString());
                
          
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving File" + ex.Message, "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void MenuFileSave_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            int index = lvTransactions.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Transaction transaction = (Transaction)lvTransactions.Items[index];
            try
            {
                Globals.db.DeleteTransactionById(transaction.Id);
                refreshTransactionList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
        }
    }
}
