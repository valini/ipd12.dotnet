﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccountDB
{
    public class Transaction
    {



        public int _id;
        public double _deposit;
        public double _withdrawal;
        public DateTime _transactionDate;

        public Transaction(int id, double deposit, double withdrawal, DateTime transactionDate)
        {
            this.Deposit = deposit;
            this.Withdrawal = withdrawal;
            this.Id = id;
            this.TransactionDate = transactionDate;
        }

        public Transaction()
        {
            this.Deposit = Deposit;
            this.Withdrawal = Withdrawal;
            this.Id = Id;
            this.TransactionDate = TransactionDate;
        }



        public int Id
        {



            get
            {

                return _id;
            }

            set
            {
                _id = value;

            }

        }

       
        public double Deposit
        {
            get
            {
                return _deposit;
            }
            set
            {

                if (value < 0)
                {
                    throw new ArgumentException("Deposit must be a positive value");
                }
                
                _deposit = value;
            }
        }

       
        public double Withdrawal
        {
            get
            {
                return _withdrawal;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Deposit must be a positive value");
                }
                _withdrawal = value;
            }
        }

       
        public DateTime TransactionDate
        {
            get
            {
                return _transactionDate;
            }
            set
            {
               
                _transactionDate = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}: +${1} $-{2}, on {3}", Id, Deposit, Withdrawal, TransactionDate);
        }

    }
}
