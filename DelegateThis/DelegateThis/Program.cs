﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateThis
{
    class Program

    {

        static void PrintSimple(String line) {
            Console.WriteLine("SIMPLE:" + line);
        }
        static void PrintFancy(String line)
        {
            Console.WriteLine("Fancy:" + line, line.Length);

        }
        static void PrintToFile(String data)
        {

            File.AppendAllText("output.txt", data + "\n");

        }
       //this is the name of the type of the delegate, declaring the type, it can point to a method that takes in a string and return void
        delegate void PrinterMethodType(String s);


        static void Main(string[] args)
        {
            //printer is a variable of type PrinterMethodType
            PrinterMethodType printer=null;
            //variable printer point to the method PrintSimple
            printer = PrintSimple;
            printer += PrintFancy;
            printer += PrintToFile;
            //calling the method using the variable as a pointer
            printer("Hello via delegate");

            //printer = PrintFancy;
            //printer("Hello via delegate");

            //remove a method
            printer -= PrintFancy;

            Console.ReadLine();




        }
    }
}
