﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewPeopleDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database db;
        public MainWindow()
        {
            try
            {
                db = new Database();
                InitializeComponent();
                refreshPeopleList();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.StackTrace);
                MessageBox.Show("Error opening database connection: " + e.Message);
                Environment.Exit(1);
            }
        }
        private void refreshPeopleList()
        {
            lvPeople.ItemsSource = db.GetAllPeople();
            // Refresh not needed, when assigning ItemsSource Refresh is triggered
            // lbPeople.Items.Refresh();
        }
        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lvPeople.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Person person = (Person)lvPeople.Items[index];
            lblId.Content = person.ID + "";
            tbName.Text = person.Name;
            tbAge.Text = person.Age + "";
            slHeight.Value = person.Height;

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            int index = lvPeople.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Person person = (Person)lvPeople.Items[index];
            try
            {
                db.DeletePersonById(person.ID);
                refreshPeopleList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            int index = lvPeople.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Person person = (Person)lvPeople.Items[index];
            try
            {
                person.Name = tbName.Text;
                person.Age = int.Parse(tbAge.Text);
                person.Height = slHeight.Value;
                db.UpdatePerson(person);
                refreshPeopleList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }

        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                int age = int.Parse(tbAge.Text);
                double height = slHeight.Value;
                Person p = new Person(0, name, age, height);
                db.AddPersonID(p);
                refreshPeopleList();


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Invalid data entered " + ex.Message);
            }


        }

        private void slHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Console.WriteLine("Height : {0}", slHeight.Value.ToString());
            //string height = slHeight.Value.ToString();
            lblHeight.Content = string.Format("{0:00.00}", slHeight.Value);
        }
    }
}
