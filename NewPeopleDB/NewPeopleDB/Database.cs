﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewPeopleDB
{
    class Database
    {
        SqlConnection conn;
        public Database()
        {
            /*  conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Valini\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30");
             conn.Open();*/
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "den1.mssql4.gear.host";
            builder.UserID = "valdatabase";
            builder.Password = "Uw5lf_-q3Y4k";
            builder.InitialCatalog = "valdatabase";
            conn = new SqlConnection(builder.ConnectionString);
            conn.Open();
        }


        public void AddPerson(Person person)
        {
            // insert a records
            SqlCommand insertCommand = new SqlCommand("INSERT INTO People (Name, Age,Height) VALUES (@Name, @Age, @Height)", conn);
            insertCommand.Parameters.AddWithValue("@Name,", person.Name);
            insertCommand.Parameters.AddWithValue("@Age,", person.Age);
            insertCommand.Parameters.AddWithValue("@Height," , person.Height);
           
            insertCommand.ExecuteNonQuery();
        }

        public int AddPersonID(Person p)
        {
            string sql = "INSERT INTO People (Name, Age, Height) VALUES (@Name, @Age, @Height);  " +
                "SELECT CONVERT(int, SCOPE_IDENTITY());";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@Name", p.Name);
                cmd.Parameters.AddWithValue("@Age", p.Age);
                cmd.Parameters.AddWithValue("@Height", p.Height);
                int id = (int)cmd.ExecuteScalar();
                return id;
            }
        }

        public List<Person> GetAllPeople() 
        {
            
            List<Person> peopleList = new List<Person>();
            using (SqlCommand command = new SqlCommand("SELECT * FROM People", conn))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string name = (string)reader["Name"];
                    int age = (int)reader["Age"];
                    double height = (double)reader["Height"];
                    Person person = new Person(id, name, age, height);
                    peopleList.Add(person);
                 
                   
                }
            }
            return peopleList;


        }

        public void UpdatePerson(Person person)
        {
           /*
            SqlCommand updateCommand = new SqlCommand("UPDATE People SET Name=@Name, Age=@Age,Height=@Height WHERE ID=@ID", conn);
            updateCommand.Parameters.AddWithValue("@Name,", person.Name);
            updateCommand.Parameters.AddWithValue("@Age,", person.Age);
            updateCommand.Parameters.AddWithValue("@Height,", person.Height);
            updateCommand.Parameters.AddWithValue("@ID,", person.ID);
            updateCommand.ExecuteNonQuery();*/
            string sql = "UPDATE People SET Name = @Name, Age = @Age, Height = @Height WHERE ID=@ID";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@ID", person.ID);
            cmd.Parameters.AddWithValue("@Name", person.Name);
            cmd.Parameters.AddWithValue("@Age", person.Age);
            cmd.Parameters.AddWithValue("@Height", person.Height);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();


        }
        public void DeletePersonById(int id) {
            
            //SqlCommand deleteCommand = new SqlCommand("DELETE FROM People WHERE //ID=@ID", conn);
            //deleteCommand.Parameters.AddWithValue("@ID,", id);

            string sql = "DELETE FROM People WHERE ID=@ID";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();


        }


    }
}
