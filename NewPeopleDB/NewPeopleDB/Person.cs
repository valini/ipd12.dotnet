﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NewPeopleDB
{
    class Person
    {
        public Person(int id, string name, int age, double height)
        {
            this.Name = name;
            this.Age = age;
            this.ID = id;
            this.Height = height;
        }

       

        private int _id;
        public int ID
        {



            get
            {
                
                return _id;
            }

            set
            {
                _id = value;

            }

        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z]{2,50}$").Success)
                {
                    throw new ArgumentException("Name must be 2-50 characters long");
                }
                _name = value;
            }
        }

        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 1 || value > 150)
                {
                    throw new ArgumentException("Age must be 1-150");
                }
                _age = value;
            }
        }

        private double _height;
        public double Height
        {
            get
            {
                return _height;
            }
            set
            {
                if (value < 30 || value > 300)
                {
                    throw new ArgumentException("Height must be 30-300");
                }
                _height = value;
            }
        }

    }
}
