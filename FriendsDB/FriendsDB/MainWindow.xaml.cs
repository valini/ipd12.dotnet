﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FriendsDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window

    {
        SqlConnection conn;
        List<Friend> friendsList = new List<Friend>();
        public MainWindow()
        {
            try
            {
                SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Valini\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30");
                conn.Open();
                InitializeComponent();
                lvFriends.ItemsSource = friendsList;

            }
            catch (SqlException sqlEx)
            {
                Console.WriteLine("Database Error: " + sqlEx.Message);
            }

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {

            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Valini\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();

            String name1 = tbName.Text;
            if (name1 == "")
            {
                return;
            }

            // addFriendIntoDB(name);
            SqlCommand insertCommand = new SqlCommand("INSERT INTO Friends (Name) VALUES (@Name)", conn);
            insertCommand.Parameters.AddWithValue("@Name", name1);
            insertCommand.ExecuteNonQuery();
            tbName.Text="";
            //loadFriendsList();
            friendsList.Clear();
            // select all records
            SqlCommand command = new SqlCommand("SELECT * FROM Friends", conn);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (reader.GetInt32(0));
                    string name = (reader.GetString(1));
                    Console.WriteLine(id);
                    Console.WriteLine(name);
                    Friend friend = new Friend(id, name);
                    friendsList.Add(friend);
                    lvFriends.ItemsSource=friendsList;
                    lvFriends.Items.Refresh();
                }
            }



        }

        public void addFriendIntoDB(String name)
        {
            // insert a records
            SqlCommand insertCommand = new SqlCommand("INSERT INTO Friends (Name) VALUES (@Name)", conn);
            insertCommand.Parameters.AddWithValue("@Name", name);
            insertCommand.ExecuteNonQuery();
        }

        public void loadFriendsList() {
            lvFriends.Items.Clear();
            // select all records
            SqlCommand command = new SqlCommand("SELECT * FROM Friends", conn);
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id= (reader.GetInt32(0));
                    string name=(reader.GetString(1));
                    Friend friend = new Friend(id, name);
                    friendsList.Add(friend);
                    lvFriends.Items.Add(friendsList);
                }
            }



        }
    }
}

