﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FriendsDB
{
    class Friend
    {
        private int _id;
        private string _name;

        public Friend(int id,string name)
        {
            this.Name = name;
            this.ID = id;
            
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (!Regex.Match(value, "^[a-zA-Z]{2,50}$").Success)

                {
                    throw new InvalidDataException("Name must be between 2 and 50 characters");
                }
                _name = value;
            }
        }
        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
               
                _id = value;
            }
        }
    }
}
