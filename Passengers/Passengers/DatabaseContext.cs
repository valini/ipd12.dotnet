﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Passengers
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=PassengerDatabase")
        {
        }

        public virtual DbSet<Passenger> Passengers { get; set; }


    }
}
