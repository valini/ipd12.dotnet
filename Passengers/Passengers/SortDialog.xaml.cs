﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Passengers
{
    /// <summary>
    /// Interaction logic for SortDialog.xaml
    /// </summary>
    public partial class SortDialog : Window
    {
        public SortDialog()
        {
            InitializeComponent();
        }

        public string SortBy
        {
            get
            {

                String sort = "";
                if (rbName.IsChecked == true)
                {

                    sort = "Name";
                }

                else if (rbPassport.IsChecked == true)
                {

                    sort = "Passport";
                }

                else if (rbDestination.IsChecked == true)
                {

                    sort = "Destination";
                }
                else if (rbDepartureDate.IsChecked == true)
                {

                    sort = "DepartureDate";
                }
                return sort;



            }
        }

        private void btApplySort_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btCancelSort_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
