﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Passengers
{
    public class Passenger

 
    {
        [Key]
        public int PassengerId { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(10)]
        public string Passport { get; set; }

        [MaxLength(100)]
        public string Destination { get; set; }

        public DateTime DepartureDate { get; set; }


    }
}
