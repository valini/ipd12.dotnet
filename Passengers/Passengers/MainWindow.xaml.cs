﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        DatabaseContext ctx;
        public MainWindow()
        {
            ctx = new DatabaseContext();
            InitializeComponent();
            refreshPassengerList();
        }

        private void refreshPassengerList()
        {
            //select GetAllMethod
            var passengers = (from r in ctx.Passengers select r).ToList();
            lvPassengers.ItemsSource = passengers;
            //lvPassengers.Items.Refresh();

        }

        private void lvPassengers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

           
        }


        private void MenuItem_DeleteClick(object sender, RoutedEventArgs e)
        {
            int index = lvPassengers.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Passenger passenger = (Passenger)lvPassengers.Items[index];
            try
            {
                int PassengerId = passenger.PassengerId;

                //int.TryParse(PersonIdS, out int PersonId);

                var passengers = (from r in ctx.Passengers where r.PassengerId == PassengerId select r).ToList();
                //if there is no such id in database
                if (passengers.Count == 0)
                {
                    Console.WriteLine("Record for deletion not found");
                }
                else
                {
                    Passenger p = passengers[0];
                    ctx.Passengers.Remove(p);
                    ctx.SaveChanges();

                }

                refreshPassengerList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDialog = new AddEditDialog(null);

            if (addEditDialog.ShowDialog() == true)
            {

               refreshPassengerList();
            }
        }

        private void lvPassengers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passenger p = (Passenger)lvPassengers.SelectedItem;
            if (p == null)
            {
                return;
            }
            AddEditDialog addEditDialog = new AddEditDialog(p);
            if (addEditDialog.ShowDialog() == true)
            {

                refreshPassengerList();
            }


        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Passenger> passengerList = (from r in ctx.Passengers select r).ToList();
            String word = tbSearch.Text;
            if (word != "")
            {
                var result = from t in passengerList where t.Name.Contains(word) || t.Destination.Contains(word)
                             select t;
                passengerList = result.ToList();

            }
            lvPassengers.ItemsSource = passengerList;
        }

        private void btSort_Click(object sender, RoutedEventArgs e)
        {
            SortDialog sortDialog = new SortDialog();

            if (sortDialog.ShowDialog() == true)
            {
                List<Passenger> passengerList = (from r in ctx.Passengers select r).ToList();
                string sort = sortDialog.SortBy;

                switch (sort)
                {
                    case "Name":
                        var pList = (from p in passengerList
                                     orderby p.Name
                                     select p).ToList();
                        passengerList = pList;
                        break;
                    case "Passport":
                        var pList1 = (from p in passengerList
                                     orderby p.Passport
                                     select p).ToList();
                        passengerList = pList1;
                        break;
                    case "Destination":
                        var pList2 = (from p in passengerList
                                     orderby p.Destination
                                     select p).ToList();
                        passengerList = pList2;
                        break;
                    case "DepartureDate":
                        var pList3 = (from p in passengerList
                                     orderby p.DepartureDate
                                     select p).ToList();
                        passengerList = pList3;
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }

               /* if (sort == "Name") {
                    var pList = (from p in passengerList
                                 orderby p.Name
                                 select p).ToList();*/
                   
                    lvPassengers.ItemsSource = passengerList;



                
        }

        }
    }
}
