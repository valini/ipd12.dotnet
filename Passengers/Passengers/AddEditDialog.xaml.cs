﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Passengers
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {  DatabaseContext ctx;
        private Passenger currentPassenger;
        public AddEditDialog(Passenger p)
        {
            currentPassenger = p;
            ctx = new DatabaseContext();
            InitializeComponent();
            if (currentPassenger == null)
            {

                btSave.Content = "Add New";
            }
            else
            {
                lblId.Content = p.PassengerId + "";
                tbName.Text = p.Name;
                tbPassport.Text = p.Passport;
                tbDestination.Text = p.Destination;
                dpDepartureDate.SelectedDate = p.DepartureDate;
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Passenger p = (currentPassenger == null ? new Passenger() : currentPassenger);



                string name = tbName.Text;
                if (!Regex.Match(name, @"^[A-Za-z]{1,100}$").Success)
                {
                    MessageBox.Show("Please Enter Correct Name: ");
                    return;
                }

                //passport
                string passport = tbPassport.Text;
                if (!Regex.Match(passport, @"^[a-zA-Z]{2,2}[0-9]{6,6}$").Success)
                {
                    MessageBox.Show("Please Enter Correct Passport: ");
                    return;
                }

                //destination
                string destination = tbDestination.Text;
                if (!Regex.Match(destination, @"^[A-Za-z]{1,100}$").Success)
                {
                    MessageBox.Show("Please Enter Correct Destination: ");
                    return;
                }
                //date

                if (dpDepartureDate.SelectedDate == null)
                {
                    MessageBox.Show("Please pick a date");
                    return;
                }
                DateTime departureDate = (DateTime)dpDepartureDate.SelectedDate;

                if (currentPassenger == null)
                {//add-insert

                    //Globals.db.AddTodo(todo);
                    p = new Passenger { Name = name, Passport = passport, Destination = destination, DepartureDate = departureDate };
                    ctx.Passengers.Add(p); // persist - schedule Person to be inserted into database table
                    ctx.SaveChanges(); // flush - force all scheduled operations to be carried out in database
                }
                else
                {//update


                    string PassengerIdS = lblId.Content.ToString();

                    int.TryParse(PassengerIdS, out int PassengerId);
                    var passengers = (from r in ctx.Passengers where r.PassengerId == PassengerId select r).ToList();
                    //if there is no data with this id in the database
                    if (passengers.Count == 0)
                    {
                        Console.WriteLine("Record for update not found");
                    }
                    else
                    {//else there is one record in the database
                        p = passengers[0];
                        p.Name = tbName.Text;
                        p.Passport = tbPassport.Text;
                        p.Destination = tbDestination.Text;
                        p.DepartureDate = (DateTime)dpDepartureDate.SelectedDate;
                        // entity is tracked/attached, meaning this will cause an update to be scheduled-saveChanges means the update is waiting or scheduled to be flushed or executed
                        ctx.SaveChanges();


                    }

                }
                DialogResult = true;

            }



            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Invalid data entered " + ex.Message);
            }

        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            try { 
            string PassengerIdS = lblId.Content.ToString();

            int.TryParse(PassengerIdS, out int PassengerId);

            var passengers = (from r in ctx.Passengers where r.PassengerId == PassengerId select r).ToList();
            //if there is no such id in database
            if (passengers.Count == 0)
            {
                Console.WriteLine("Record for deletion not found");
            }
            else
            {
                Passenger p = passengers[0];
                ctx.Passengers.Remove(p);
                ctx.SaveChanges();
                    lblId.Content = "";
                    tbName.Text = "";
                    tbPassport.Text = "";
                    tbDestination.Text = "";
                    dpDepartureDate.SelectedDate = null;
                    
                }
                DialogResult = true;
            
        }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }


}

private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
