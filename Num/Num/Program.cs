﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Num
{
    class Program
    {
        static void Main(string[] args)
        {

            //Declare a list to store the integers
            List<int> list = new List<int>();

            while (true)
            {
                Console.WriteLine("Enter a positive number, 0 to end yes: ");
                string line = Console.ReadLine();
                // int value = int.Parse(line);
                int value;
                //Take the string and parse it, and if it is not a value(False), it is not an integer
                if (int.TryParse(line, out value) == false)
                {
                    Console.WriteLine("Not a valid integer, try again");
                    continue;
                }
                if (value <= 0)
                {
                    Console.WriteLine("Done entering data");
                    break;
                }
                list.Add(value);
            }




            double sum = 0.0;
            foreach (int i in list)
            {

                sum += i;

            }
            Console.WriteLine("Sum: " + sum);

            double avg = (double)sum / list.Count;

            Console.WriteLine("Average: " + avg);

            /*
            int Max = int.MinValue;
            int  Min = int.MaxValue;

            foreach (int x in list)
            {
                if (Max < x)
                {
                    Max = x;
                }
                if (Min > x)
                {
                    Min = x;
                }
            }

            Console.WriteLine("Minimum value is " + Min);
        
            Console.WriteLine("Maximum value is " + Max);
          */

            //Maximum
            list.Sort();
            Console.WriteLine("Maximum is {0}", list[list.Count - 1]);
            //Median
            //if list is odd
            double median;
            if (list.Count % 2 == 1)
            { // odd number of elements - take middle one
                median = list[list.Count / 2];
            }
            else
            {
                median = (list[list.Count / 2] + list[list.Count / 2 - 1]) / 2.0;
            }
            
            Console.WriteLine("Median is {0}", median);
            
            //Standard deviation
            
            double total=0.0;
            foreach (int v in list)
            {
                 total += Math.Pow((v) - avg, 2.0);
            }
            Console.WriteLine(total);
            double variance = total / list.Count;
            Console.WriteLine("Variance is {0}", variance);
            double sd = Math.Pow(variance, 0.5);
            Console.WriteLine("Standard Deviation is {0}", sd);
            // write numbers to file, semicolon-separated
            string data = String.Join(";", list);
            try
            {
                File.WriteAllText("output.txt", data);
            }
            catch (IOException ex)
            {
                Console.Write("Error occured when writing data\n" + ex);
            }
            //
            Console.ReadLine();
        }
    }
}
