﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFPeople
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Person p = new Person { Name = "Jerry", Age = rand.Next(1, 150) };
            //opening database connection
            using (DatabaseContext ctx = new DatabaseContext()) {
                //insert
                ctx.People.Add(p);//persist schedule Person to be inserted into database table
                //run query
                ctx.SaveChanges();//Flush - force all scheduled opretations to be carried out in database
                Console.WriteLine("Person persisted");

                //select
                var people = (from r in ctx.People select r).ToList();
                foreach (var pp in people)
                {

                    Console.WriteLine("P({0}):{1}, {2})", pp.personId, pp.Name, pp.Age);
                }
            }
            Console.ReadLine();

        }
    }
}
