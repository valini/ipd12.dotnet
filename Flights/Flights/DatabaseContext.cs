﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Flights
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=FlightsDatabase")
        {
        }

        public virtual DbSet<Flight> Flights { get; set; }


    }
}
