﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flights
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DatabaseContext ctx;
        public MainWindow()
        {
            ctx = new DatabaseContext();
            InitializeComponent();
            refreshFlightsList();
        }

        private void refreshFlightsList()
        {
            //select GetAllMethod
            var flights = (from r in ctx.Flights select r).ToList();
            lvFlights.ItemsSource = flights;
            lblCursorPosition.Text = ("Total Flights :" + lvFlights.Items.Count );

        }

        private void miAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDialog = new AddEditDialog(null);

            if (addEditDialog.ShowDialog() == true)
            {

                refreshFlightsList();
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            int index = lvFlights.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Flight flight = (Flight)lvFlights.Items[index];
            try
            {
                int FlightId = flight.FlightId;

                //int.TryParse(PersonIdS, out int PersonId);

                var flights = (from r in ctx.Flights where r.FlightId == FlightId select r).ToList();
                //if there is no such id in database
                if (flights.Count == 0)
                {
                    MessageBox.Show("Record for deletion not found");

                }
                else
                {
                    Flight p = flights[0];
                    ctx.Flights.Remove(p);
                    ctx.SaveChanges();

                }

                refreshFlightsList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }



        }

        private void MenuFileSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Flight t = (Flight)lvFlights.SelectedItem;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
                if (saveFileDialog.ShowDialog() == true)
                    File.AppendAllText(saveFileDialog.FileName, t.ToString());


            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving File" + ex.Message, "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void lvFlight_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Flight p = (Flight)lvFlights.SelectedItem;
            if (p == null)
            {
                return;
            }
            AddEditDialog addEditDialog = new AddEditDialog(p);
            if (addEditDialog.ShowDialog() == true)
            {

                refreshFlightsList();
            }

        }
    }
}
