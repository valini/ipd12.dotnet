﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Flights
{
    public class Flight
    {
        [Key]
        public int FlightId { get; set; }

       
        public DateTime OnDay { get; set; }

        [MaxLength(5)]
        public string FromCode { get; set; }

        [MaxLength(5)]
        public string ToCode { get; set; }

       
        public Type Type { get; set; }

        
        public int Passengers { get; set; }
        public override string ToString()
        {
            return string.Format("Flight{0}: {1} {2} {3}, on {4}", FlightId, FromCode, ToCode, Type, OnDay);
        }


    }

    public enum Type
    {
        Domestic,
        International,
        Private
    }

    
}
