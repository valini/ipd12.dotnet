﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Flights
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        static DatabaseContext ctx;
        private Flight currentFlight;
        public AddEditDialog(Flight p)
        {
            currentFlight = p;
            ctx = new DatabaseContext();
            InitializeComponent();
            if (currentFlight == null)
            {

                btSave.Content = "Add New";
            }
            else
            {
                lblId.Content = p.FlightId + "";
                dpOnDay.SelectedDate = p.OnDay;
                tbFromCode.Text = p.FromCode;
                tbToCode.Text = p.ToCode;
                
                cmbType.Text = p.Type+"";
                slPassengers.Value = p.Passengers;
                
            }
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Flight p = (currentFlight == null ? new Flight() : currentFlight);

                //date
                

                if (dpOnDay.SelectedDate == null)
                {
                    MessageBox.Show("Please pick a date");
                    return;
                }

                DateTime onDay = (DateTime)dpOnDay.SelectedDate;
                //from and toCode

                string fromCode = tbFromCode.Text;
                if (!Regex.Match(fromCode, @"^[A-Z]{3,5}$").Success)
                {
                    MessageBox.Show("Please Enter Correct FromCode: ");
                    return;
                }


                string toCode = tbToCode.Text;
                if (!Regex.Match(fromCode, @"^[A-Z]{3,5}$").Success)
                {
                    MessageBox.Show("Please Enter Correct ToCode: ");
                    return;
                }



                //type
                string typeS = cmbType.Text;
                Type type = (Type)Enum.Parse(typeof(Type),typeS);


                //passengers
                int passengers = (int)slPassengers.Value;


                if (currentFlight == null)
                {//add-insert

                    //Globals.db.AddTodo(todo);
                    p = new Flight { OnDay = onDay, FromCode = fromCode, ToCode = toCode, Type=type, Passengers=passengers };
                  
                    ctx.Flights.Add(p); // persist - schedule Person to be inserted into database table
                    ctx.SaveChanges(); // flush - force all scheduled operations to be carried out in database
                }
                else
                {//update


                    string FlightIdS = lblId.Content.ToString();

                    int.TryParse(FlightIdS, out int FlightId);
                    var flights = (from r in ctx.Flights where r.FlightId == FlightId select r).ToList();
                    //if there is no data with this id in the database
                    if (flights.Count == 0)
                    {
                        MessageBox.Show("Record for update not found");
                    }
                    else
                    {//else there is one record in the database
                        p = flights[0];
                        p.OnDay = (DateTime)dpOnDay.SelectedDate;
                        p.FromCode = tbFromCode.Text;
                        p.ToCode = tbToCode.Text;
                        string typeT = cmbType.Text;
                        Type typeW = (Type)Enum.Parse(typeof(Type), typeT);
                        p.Type = typeW;//UPDATE WORKS!! ON REFRESH WHEN APP RUNS AGAIN
                        p.Passengers = (int)slPassengers.Value;
                        // entity is tracked/attached, meaning this will cause an update to be scheduled-saveChanges means the update is waiting or scheduled to be flushed or executed
                        ctx.SaveChanges();


                    }

                }
                DialogResult = true;

            }



            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Invalid data entered " + ex.Message);
            }

        }
    }
}
