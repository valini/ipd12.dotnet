﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsGrades
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                String[] lines = System.IO.File.ReadAllLines(@"..\..\grades.txt");


                System.Console.WriteLine("Contents of grades.txt = ");



                foreach (string line in lines)
                {


                    // Use a tab to indent each line of the file.
                    Console.WriteLine("\t" + line);


                }

                //List<String> list = new List<String>();
                foreach (string line in lines)
                {
                    //list.Add(line);
                    String[] lineElements = line.Split(new char[] { ':', ',' });

                    foreach (string s in lineElements)
                    {
                        if (s.Trim() != "")

                            Console.WriteLine(s);
                    }
                    double sum = 0.00;
                    for (int i = 1; i < lineElements.Length; i++)
                    {
                        sum += LetterToNumberGrade(lineElements[i]);
                    }

                    double avg = sum / (lineElements.Length - 1);
                    Console.WriteLine(lineElements[0] + "has GPA of." + avg);


                    Console.WriteLine("Press any key to exit.");
                    System.Console.ReadKey();

                }
            }

            catch (IOException ex)
            {
                Console.Write("Error reading file" + ex);
                Console.ReadLine();
            }
        }

        static double LetterToNumberGrade(string strGrade) {

            switch (strGrade)
            {
                case "A+":
                    return 4.0;

                case "A-":
                    return 3.67;

                case "B+":
                    return 3.33;

                case "B":
                    return 3.00;

                case "B-":
                    return 2.67;
                case "C+":
                    return 2.33;
                case "C":
                    return 2.00;
                case "C-":
                    return 1.67;
                case "D+":
                    return 1.33;
                case "D":
                    return 1.00;
                case "D-":
                    return 0.67;
                case "F":
                    return 0.00;

                default:
                    return 0.0;


            }



        }
    }
}
