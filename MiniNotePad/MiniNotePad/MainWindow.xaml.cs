﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MiniNotePad
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        String currentFile = "";
        public MainWindow()
        {
            InitializeComponent();
            //lblCursorPosition.Text = "No opened file";
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuFileOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    currentFile = openFileDialog.FileName;
                    tbDocument.Text = File.ReadAllText(currentFile);
                    lblCursorPosition.Text = currentFile;
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error Opening File" + ex.Message, "File Open error", MessageBoxButton.OK, MessageBoxImage.Error);

                }
            }
        }

        private void MenuFileSaveAs_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
                if (saveFileDialog.ShowDialog() == true)
                    File.WriteAllText(saveFileDialog.FileName, tbDocument.Text);
                Title = "Main Window";
                //getting the file path of the current file
                currentFile = saveFileDialog.FileName;
                //setting the text in the status bar
                lblCursorPosition.Text = currentFile;
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving File" + ex.Message, "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void tbDocument_TextChanged(object sender, TextChangedEventArgs e)
        {
            Title = "Modified** Main Window";
            //lblCursorPosition.Text = "New file";

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Title == "Modified** Main Window" && lblCursorPosition.Text == "")
            {
                MessageBoxResult result = MessageBox.Show("Would you like to save the file?", "MiniNotePad", MessageBoxButton.YesNo);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        try
                        {
                            SaveFileDialog saveFileDialog = new SaveFileDialog();
                            saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
                            if (saveFileDialog.ShowDialog() == true)
                                File.WriteAllText(saveFileDialog.FileName, tbDocument.Text);
                            Title = "Main Window";
                        }
                        catch (IOException ex)
                        {
                            MessageBox.Show("Error saving File" + ex.Message, "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        break;
                    case MessageBoxResult.No:
                        Environment.Exit(1);
                        break;

                }
            }
            if (Title == "Modified** Main Window" && (!String.IsNullOrEmpty(currentFile)))
            {
                try
                {
                    SaveFileDialog saveDialog = new SaveFileDialog();
                    saveDialog.OverwritePrompt = true;
                    File.WriteAllText(currentFile, tbDocument.Text);
                    Title = "Main Window";
                }
                catch (IOException ex) { MessageBox.Show("Error saving File" + ex.Message, "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error); }


            }
        }

        private void MenuFileSave_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(currentFile))
            {
                try
                {
                    SaveFileDialog saveDialog = new SaveFileDialog();
                    saveDialog.OverwritePrompt = true;
                    File.WriteAllText(currentFile, tbDocument.Text);
                    Title = "Main Window";
                }
                catch (IOException ex)
                { MessageBox.Show("Error saving File" + ex.Message, "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error); }
            }

            if(String.IsNullOrEmpty(currentFile)&& Title=="MainWindow") {
                MessageBox.Show("Error saving file. Text Box is empty", "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            if (String.IsNullOrEmpty(currentFile) && Title== "Modified** Main Window")
            {
                try
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "Text file (*.txt)|*.txt|C# file (*.cs)|*.cs";
                    if (saveFileDialog.ShowDialog() == true)
                        File.WriteAllText(saveFileDialog.FileName, tbDocument.Text);
                    Title = "Main Window";
                    currentFile = saveFileDialog.FileName;
                    lblCursorPosition.Text = currentFile;
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving File" + ex.Message, "File Saving error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }


        }
        private void MenuFileExit_Click(object sender, RoutedEventArgs e)
        {
            //Environment.Exit(1);
            //this method will call the window closng event
            Close();
        }
    }
}

    



    

