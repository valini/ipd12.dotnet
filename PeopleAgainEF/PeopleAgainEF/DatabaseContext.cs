﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace PeopleAgainEF
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name=PeopleAgainDatabase")
        {
        }

        public virtual DbSet<Person> People { get; set; }

    }
}
