﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PeopleAgainEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DatabaseContext ctx;
        public MainWindow()
        {
            ctx = new DatabaseContext();
            InitializeComponent();
            refreshPeopleList();


        }

        private void refreshPeopleList()
        {
            //select GetAllMethod
            var people = (from r in ctx.People select r).ToList();
            lvPeople.ItemsSource = people;

        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                if (!Regex.Match(name, @"^[A-Za-z, -]{1,50}$").Success)
                {
                    MessageBox.Show("Please Enter Correct Name: ");
                    return;
                }
                
                //int age = int.Parse(tbAge.Text);
                if (!int.TryParse(tbAge.Text, out int age))
                {
                    MessageBox.Show("Please Enter Age between 1 and 150: ");
                    return;
                }

                if (age <= 1 || age >= 150) {
                    MessageBox.Show("Please Enter Age between 1 and 150: ");
                    return;

                }
                double height = slHeight.Value;

                Person p = new Person { Name = name, Age = age, Height=height };
                ctx.People.Add(p); // persist - schedule Person to be inserted into database table
                ctx.SaveChanges(); // flush - force all scheduled operations to be carried out in database
                Console.WriteLine("Person persisted");

                
                refreshPeopleList();


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Invalid data entered " + ex.Message);
            }

        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            int index = lvPeople.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Person person = (Person)lvPeople.Items[index];
            try
            {
                

                string PersonIdS = lblId.Content.ToString();

                 int.TryParse(PersonIdS, out int PersonId);
                var people = (from r in ctx.People where r.PersonId == PersonId select r).ToList();
                //if there is no data with this id in the database
                if (people.Count == 0)
                {
                    Console.WriteLine("Record for update not found");
                }
                else
                {//else there is one record in the database
                    Person p = people[0];
                    person.Name = tbName.Text;
                    person.Age = int.Parse(tbAge.Text);
                    person.Height = slHeight.Value;
                    // entity is tracked/attached, meaning this will cause an update to be scheduled-saveChanges means the update is waiting or scheduled to be flushed or executed
                    ctx.SaveChanges();


                    refreshPeopleList();
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }
        }

        private void slHeight_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblHeight.Content = string.Format("{0:00.00}", slHeight.Value);

        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = lvPeople.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Person person = (Person)lvPeople.Items[index];
            lblId.Content = person.PersonId + "";
            tbName.Text = person.Name;
            tbAge.Text = person.Age + "";
            slHeight.Value = person.Height;


        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            int index = lvPeople.SelectedIndex;
            if (index < 0)
            {
                return;
            }
            Person person = (Person)lvPeople.Items[index];
            try
            {
                string PersonIdS = lblId.Content.ToString();

                int.TryParse(PersonIdS, out int PersonId);

                var people = (from r in ctx.People where r.PersonId == PersonId select r).ToList();
                //if there is no such id in database
                if (people.Count == 0)
                {
                    Console.WriteLine("Record for deletion not found");
                }
                else
                {
                    Person p = people[0];
                    ctx.People.Remove(p);
                    ctx.SaveChanges();
                    
                }
              
                refreshPeopleList();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database query error " + ex.Message);
            }

        }
    }
}
