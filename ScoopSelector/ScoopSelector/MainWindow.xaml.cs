﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ScoopSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            //var selectedItem = lvFlavours.SelectedItem;
            
            if (lvFlavours.SelectedItems.Count==0) {
                MessageBox.Show("No items Selected", "Choose a flavour", MessageBoxButton.OK, MessageBoxImage.Error);
                return;

            }
            String items = ((ListViewItem)(lvFlavours.SelectedItem)).Content.ToString(); lvSelected.Items.Add(items);
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            //String items = ((ListViewItem)(lvSelected.SelectedItem)).Content.ToString();

            var selectedItem = lvSelected.SelectedItem;
            lvSelected.Items.Remove(selectedItem);
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            lvSelected.Items.Clear();
        }
    }
}
