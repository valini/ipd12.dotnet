﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CustomSandwich
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        private MainWindow window;
        public InputDialog(MainWindow parent)
        {
            InitializeComponent();
            window = parent;
        }

        private void dlgSave_Click(object sender, RoutedEventArgs e)
        {
            window.lblBread.Content = cmbBread.Text;
            String veggies="";
           
            if (cbLettuce.IsChecked == true)
            {

                veggies = "Lettuce";
            }



            if (cbTomatoes.IsChecked == true)
            {

                if (veggies.Length > 0)
                {

                    veggies = veggies + "," + "Tomatoes";
                }

                else
                {
                    veggies = "Tomatoes";
                }
            }


            if (cbCucumbers.IsChecked == true)
            {

                if (veggies.Length > 0)
                {
                    veggies = veggies + "," + "Cucumber";
                }
                else
                {
                    veggies = "Cucumber";
                }

            }
            window.lblVeggies.Content = veggies;

            //Meat
            String meat = "";
            if (rbChicken.IsChecked == true)
            {

                meat = "Chicken";
            }

            else if (rbTurkey.IsChecked == true)
            {

                meat = "Turkey";
            }

            else if (rbTofu.IsChecked == true)
            {

                meat = "Tofu";
            }
            window.lblMeat.Content = meat;

            //this.DialogResult = true;
            Close();

        }
    }
}
