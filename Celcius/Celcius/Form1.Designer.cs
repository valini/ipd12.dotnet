﻿namespace Celcius
{
    partial class wnConverter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tfCelsius = new System.Windows.Forms.TextBox();
            this.tfFarenheit = new System.Windows.Forms.TextBox();
            this.btConvert = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Celcius:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Farenheit:";
            // 
            // tfCelsius
            // 
            this.tfCelsius.Location = new System.Drawing.Point(73, 26);
            this.tfCelsius.Name = "tfCelsius";
            this.tfCelsius.Size = new System.Drawing.Size(100, 20);
            this.tfCelsius.TabIndex = 2;
            // 
            // tfFarenheit
            // 
            this.tfFarenheit.Location = new System.Drawing.Point(73, 65);
            this.tfFarenheit.Name = "tfFarenheit";
            this.tfFarenheit.Size = new System.Drawing.Size(100, 20);
            this.tfFarenheit.TabIndex = 3;
            // 
            // btConvert
            // 
            this.btConvert.Location = new System.Drawing.Point(25, 124);
            this.btConvert.Name = "btConvert";
            this.btConvert.Size = new System.Drawing.Size(100, 23);
            this.btConvert.TabIndex = 4;
            this.btConvert.Text = "Convert";
            this.btConvert.UseVisualStyleBackColor = true;
            this.btConvert.Click += new System.EventHandler(this.btConvert_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(189, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "C";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(189, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "F";
            // 
            // btExit
            // 
            this.btExit.Location = new System.Drawing.Point(131, 124);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(105, 23);
            this.btExit.TabIndex = 7;
            this.btExit.Text = "Exit";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.button1_Click);
            // 
            // wnConverter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btConvert);
            this.Controls.Add(this.tfFarenheit);
            this.Controls.Add(this.tfCelsius);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "wnConverter";
            this.Text = "Temperature Convertor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tfCelsius;
        private System.Windows.Forms.TextBox tfFarenheit;
        private System.Windows.Forms.Button btConvert;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btExit;
    }
}

