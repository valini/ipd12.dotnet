﻿namespace AllInputs
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tfName = new System.Windows.Forms.TextBox();
            this.Age = new System.Windows.Forms.Label();
            this.tfAge = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // tfName
            // 
            this.tfName.Location = new System.Drawing.Point(58, 5);
            this.tfName.Name = "tfName";
            this.tfName.Size = new System.Drawing.Size(100, 20);
            this.tfName.TabIndex = 1;
            // 
            // Age
            // 
            this.Age.AutoSize = true;
            this.Age.Location = new System.Drawing.Point(16, 56);
            this.Age.Name = "Age";
            this.Age.Size = new System.Drawing.Size(29, 13);
            this.Age.TabIndex = 2;
            this.Age.Text = "Age:";
            // 
            // tfAge
            // 
            this.tfAge.Location = new System.Drawing.Point(58, 48);
            this.tfAge.Name = "tfAge";
            this.tfAge.Size = new System.Drawing.Size(100, 20);
            this.tfAge.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 261);
            this.Controls.Add(this.tfAge);
            this.Controls.Add(this.Age);
            this.Controls.Add(this.tfName);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tfName;
        private System.Windows.Forms.Label Age;
        private System.Windows.Forms.TextBox tfAge;
    }
}

