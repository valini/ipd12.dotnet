﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndexPlay
{
    class Program
    {



        class String10Storage
        {
            private string[] myData;

            public String10Storage(int size)
            {
                myData = new string[size];

                for (int i = 0; i < size; i++)
                {
                    myData[i] = "empty";
                }
            }

            public string this[long pos]
            {
                get
                {
                    return myData[pos];
                }
                set
                {
                    myData[pos] = value;
                }
            }



        }//end of class String10Storage

        class PrimeArray
        {
            private string[] myData;

            public PrimeArray(int size)
            {
                myData = new string[size];

                for (int i = 0; i < size; i++)
                {
                    myData[i] = "empty";
                }
            }

            public bool this[long pos]
            {
                get
                {
                    for (int a = 2; a <= pos / 2; a++)
                    {
                        if (pos % a == 0)
                        {
                            //Console.WriteLine(pos + " is not prime number");
                            return false;
                        }

                    }
                    return true;
                }
                /*set
                {
                    myData[pos] = value;
                }*/
            }
            public long this[int pos]
            {
                get
                {
                    long value = 1;
                    for (int a = 1; a <= pos;)
                    {
                        if (this[value])
                        {
                            if (a == pos) return value;

                            a++;
                        }
                    value++;
                    }
                    return -1;
                }
                /*set
                {
                    myData[pos] = value;
                }*/
            }



        }//end of class PrimeArray

        static void Main(string[] args)
        {
                String10Storage Team = new String10Storage(10);

                Team[0] = "Rocky";

                Team[1] = "Teena";

                Team[2] = "Ana";

                Team[3] = "Victoria";

                Team[4] = "Yani";

               // Team[5] = "Mary";

                //Team[6] = "Gomes";

                //Team[7] = "Arnold";

                //Team[8] = "Mike";

                //Team[9] = "Peter";

                for (int i = 0; i < 10; i++)

                {

                    Console.WriteLine(Team[i]);

                }

            //Prime Numbers
            int size = 10;
            PrimeArray prime = new PrimeArray(size);
            for (long i = 0; i < size; i++)
            {
                if (prime[i])
                {
                    Console.WriteLine("PA[" + i + "] returns true");
                }else
                {
                    Console.WriteLine("PA[" + i + "] returns false");
                }
            }

            //Prime Numbers with modifications
            int size1 = 20;
            PrimeArray primeM = new PrimeArray(size1);
            for (int i = 1; i < size1; i++)
            {
                Console.WriteLine("PA[" + i + "] returns " + primeM[i]);
            }

            Console.ReadLine();


            
        }
    }
}
