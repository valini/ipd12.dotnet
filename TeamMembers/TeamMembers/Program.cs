﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamMembers
{
    class Program
    {
        static Dictionary<String, List<String>> MemberInTeams = new Dictionary<String, List<String>>();


        static void RegisterMemberInTeam(string member, string teamName)
        {
            if (MemberInTeams.ContainsKey(member))
            {
                List<String> list = MemberInTeams[member];
                list.Add(teamName);
            }
            else
            { // create the entry
                List<String> newList = new List<String>();
                newList.Add(teamName);
                MemberInTeams.Add(member, newList);
            }
        }


        static void Main(string[] args)
        {
            string[] linesArray = File.ReadAllLines(@"..\..\teamMembers.txt");
            foreach (String line in linesArray)
            {
                string[] splitOne = line.Split(':');
                string teamName = splitOne[0];
                string[] teamMembersArray = splitOne[1].Split(',');
                foreach (String member in teamMembersArray)
                {
                    RegisterMemberInTeam(member, teamName);
                }
            }
            // display the data
            foreach (string member in MemberInTeams.Keys)
            {
                List<String> list = MemberInTeams[member];
                Console.Write(member + ": ");
                foreach (string teamName in list)
                {
                    Console.Write(teamName + ", ");
                }
                Console.WriteLine();
            }
            //
            Console.ReadKey();



        }
    }
}
